from dtw import *
import numpy as np
import pandas as pd

def call_dtw(query,ref,parametres,pente,distance,open_begin = False):
    '''
    

    Parameters
    ----------
    query : DataFrame
        Données à recaler.
    ref : DataFrame
        Référence.
    parametres : List of String
        Listes des paramètres utiliser pour le recalage.
    pente : Int
        Contrainte de pente.
    distance : String
        Distance utilisé.
    open_begin : Bool, optional
        Si vrai, on effectue le dtw ouvert au début. The default is False.

    Returns
    -------
    a : DTW object
        Output de la fonction dtw.

    '''
    if(not open_begin):
        if(pente == 0):
            a = dtw(np.asmatrix(query[parametres]),
                    np.asmatrix(ref[parametres]),
                    step_pattern = symmetricP0,
                    dist_method = distance)
        elif(pente == 1/2):
            a = dtw(np.asmatrix(query[parametres]),
                    np.asmatrix(ref[parametres]),
                    step_pattern = symmetricP05,
                    dist_method = distance)
        elif(pente== 1):
            a = dtw(np.asmatrix(query[parametres]),
                    np.asmatrix(ref[parametres]),
                    step_pattern = symmetricP1,
                    dist_method = distance)    
        else:
            a = dtw(np.asmatrix(query[parametres]),
                    np.asmatrix(ref[parametres]),
                    step_pattern = symmetricP2,
                    dist_method = distance)
            
    else:
        if(pente == 0):
            a = dtw(np.asmatrix(query[parametres]),
                    np.asmatrix(ref[parametres]),
                    step_pattern = asymmetricP0,
                    dist_method = distance,
                    open_begin = True)
        elif(pente == 1/2):
            a = dtw(np.asmatrix(query[parametres]),
                    np.asmatrix(ref[parametres]),
                    step_pattern = asymmetricP05,
                    dist_method = distance,
                    open_begin = True)
        elif(pente== 1):
            a = dtw(np.asmatrix(query[parametres]),
                    np.asmatrix(ref[parametres]),
                    step_pattern = asymmetricP1,
                    dist_method = distance,
                    open_begin = True)    
        else:
            a = dtw(np.asmatrix(query[parametres]),
                    np.asmatrix(ref[parametres]),
                    step_pattern = asymmetricP2,
                    dist_method = distance,
                    open_begin = True)

    return a 

def segDTW(query, ref, index_reference = True, 
           parametres1 = ["Ph_sd_curve","Temp_curve","Temp_sd_curve"],
           parametres2 = ["Ph_sd_curve","Temp_sd_curve"],
           parametres3 = ["Ph_sd_curve","Temp_sd_curve"],
           parametres4 = ["Ph_sd_curve","Temp_sd_curve","Ph_curve"],
           pente = [2,2,2,1], distance = "Canberra",
           Keep_Dech = True, Keep_Pickle = True, Keep_Fin_Pickle = True, Keep_Tannage = True):

    if(Keep_Dech):
        tmp_query = query[query.Period == "Dechaulage"].reset_index(drop = True)
        tmp_ref = ref[ref.Period == "Dechaulage"]
        
        a = call_dtw(tmp_query,tmp_ref,parametres1,pente[0],distance,open_begin = True)
        wq = warp(a, index_reference = index_reference)
        if (index_reference == True):
            wq = np.append(wq,tmp_ref.shape[0]-1)
        else:
            wq = np.append(wq,tmp_query.shape[0]-1)
        wq_Dechaulage = wq
    else:
        wq_Dechaulage = []

    if(Keep_Pickle):
        tmp_query = query[query.Period == "Pickle"].reset_index(drop = True)
        tmp_ref = ref[ref.Period == "Pickle"]
        
        a = call_dtw(tmp_query,tmp_ref,parametres2,pente[1],distance)
        wq = warp(a, index_reference = index_reference)
        if (index_reference == True):
            wq = np.append(wq,tmp_ref.shape[0]-1)
        else:
            wq = np.append(wq,tmp_query.shape[0]-1)
        wq_Pickle = wq
    else:
        wq_Pickle= []

    if(Keep_Fin_Pickle):
        tmp_query = query[query.Period == "Fin_Pickle"].reset_index(drop = True)
        tmp_ref = ref[ref.Period == "Fin_Pickle"]
        
        a = call_dtw(tmp_query,tmp_ref,parametres3,pente[2],distance)
        wq = warp(a, index_reference = index_reference)
        if (index_reference == True):
            wq = np.append(wq,tmp_ref.shape[0]-1)
        else:
            wq = np.append(wq,tmp_query.shape[0]-1)
        wq_Fin_Pickle = wq
    else:
        wq_Fin_Pickle = []

    if(Keep_Tannage):
        tmp_query = query[(query.Period == "Pretannage") | (query.Period == "Tannage")].\
            reset_index(drop = True)
        tmp_ref = ref[(ref.Period == "Pretannage") | (ref.Period == "Tannage")]
        
        a = call_dtw(tmp_query,tmp_ref,parametres4,pente[3],distance)
        wq = warp(a, index_reference = index_reference)
        if (index_reference == True):
            wq = np.append(wq,tmp_ref.shape[0]-1)
        else:
            wq = np.append(wq,tmp_query.shape[0]-1)
        wq_Tannage = wq
    else: 
        wq_Tannage = []
        
    return wq_Dechaulage, wq_Pickle, wq_Fin_Pickle,wq_Tannage


def allDTW(query, ref, pente=2,index_reference = True, 
           parametres=["Ph_sd_curve", "Temp_sd_curve"], distance="Canberra", open_begin=True):
    a = call_dtw(query, ref, parametres, pente, distance, open_begin)
    wq = warp(a, index_reference = index_reference)
    if (index_reference == True):
        wq = np.append(wq, ref.shape[0] - 1)
    else:
        wq = np.append(wq, query.shape[0] - 1)

    return wq