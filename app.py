from datetime import datetime as dt
import dash
import dash_html_components as html
import dash_core_components as dcc
import dash_design_kit as ddk
import dash_bootstrap_components as dbc
from dash.dependencies import Input, Output
import dash_daq as daq
from datetime import datetime
import dash_table
import pandas as pd
import numpy as np
import plotly.express as px
import plotly.graph_objs as go
from Appfuns import segDTW, allDTW
import re


external_stylesheets = ["https://codepen.io/chriddyp/pen/bWLwgP.css"]
app = dash.Dash(__name__) 
server = app.server
app.config.suppress_callback_exceptions = True


Data_Foulon1 = pd.read_csv("./assets/data_curve.csv")
Foulon1 = pd.read_csv("./assets/LotF1.csv")
cc = pd.read_csv("./assets/cc_Global.csv")
IC_durrees = pd.read_csv("./assets/IC_Durée.csv")
#Alertes = pd.read_csv("./assets/Alertes.csv", sep=";")
Alertes = pd.read_csv("./assets/Alertes_Global.csv")

# Création des colonnes d'icones dans la base des alertes
for col in Alertes.columns[1:5]:
    Alertes[col + "_icon"] = Alertes[col].apply(lambda x:'✅' if x else ('❌'))
for col in Alertes.columns[5:9]:
    Alertes[col + "_icon"] = Alertes[col].apply(lambda x: '❌' if x else ('✅'))

# Création des colonnes de pourcentage dans la base des alertes
cols_pc = Alertes.columns[9:17]
for col in cols_pc:
    Alertes[col + "_pc"] = Alertes[col].round(1).astype(str) + "%"
# Suppression des "nan%"
Alertes = Alertes.replace(to_replace ="nan%", value ="-")

styles = {
    'pre': {
        'border': 'thin lightgrey solid',
        'overflowX': 'scroll'
    }
}

tabs_styles = {
    'height': '50px'
}
tab_style = {
    'borderBottom': '0px solid #d6d6d6',
    'padding': '6px'
}

tab_selected_style = {
    'borderTop': '0px solid #d6d6d6',
    'borderBottom': '1px solid #d6d6d6',
    'backgroundColor': '#119DFF',
    'color': 'white',
    'padding': '6px',
    'fontWeight': 'bold'
}


app.layout = ddk.App([
        ddk.Header(style={'height' : '10vh', 'background' : '#1A77BD', "marginBottom" : "0px"}, children = [
        ddk.Logo(app.get_asset_url("logo.png"),style={'position' : 'right'}),
        ddk.Title('Tannerie 4.0 - Capteurs Foulons', style={'align' : 'center', 'color' : 'white'})
    ]),

    
# ---------------- #
# --- Onglet 1 --- #
# ---------------- #
    dcc.Tabs(style = tabs_styles, children = [
        dcc.Tab(label='Temps Réel', style=tab_style, selected_style=tab_selected_style, children=[
            ddk.Sidebar(
                style = {'width' : '20vw', 'background' : '#002060' , 'height' : '85vh'},
                children=[
                    ddk.ControlCard(
                        type='flat',
                        style={'background' : '#002060'},
                        children=[
                            html.Label(["Foulon :"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.Dropdown(
                                id='o1_Foulon',
                                options=[{'label': i, 'value': i} for i in Foulon1.NomMachine.unique()],
                                value=Foulon1.NomMachine.unique()[0], 
                                clearable = False,
                                style = {'color' : 'black', "background-color": "white"}
                            ),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br()
                        ]
                    )    
                ]
            ),

            ddk.SidebarCompanion([
                html.Div([
                    html.Div([
                        dcc.Graph(id='o1_Plot_pH', 
                            config={
                                "displaylogo": False,
                                'modeBarButtonsToRemove': ['lasso2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines', 'resetScale2d']
                        })
                    ], className = "twelve columns")
                    
                ], className="row"),
                html.Div([
                    html.Div([
                        dcc.Graph(id='o1_Plot_Temp', 
                        config={
                                "displaylogo": False,
                                'modeBarButtonsToRemove': ['lasso2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines', 'resetScale2d']
                        })
                    ], className = "twelve columns")    
                ], className="row"),
            ])
        ]),

# ---------------- #
# --- Onglet 2 --- #
# ---------------- #

        dcc.Tab(label='Analyse d\'un lot', style=tab_style, selected_style=tab_selected_style, children=[
            ddk.Sidebar(
                style = {'width' : '20vw', 'background' : '#002060' , 'height' : '85vh'},
                children=[
                    ddk.ControlCard(
                        type='flat',
                        style={'background' : '#002060'},
                        children=[
                            html.Label(["Période"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.DatePickerRange(
                                id='o2_date_range', 
                                display_format="DD/MM/YY",
                                min_date_allowed=pd.to_datetime(Foulon1['HeureDebut']).dt.date.min(),
                                max_date_allowed=pd.to_datetime(Foulon1['HeureDebut']).dt.date.max(),
                                initial_visible_month=pd.to_datetime(Foulon1['HeureDebut']).dt.date.max(),
                                with_portal=True,
                                clearable = False,
                                first_day_of_week = 1,
                                end_date = pd.to_datetime(Foulon1['HeureDebut']).dt.date.max(), 
                                start_date = pd.to_datetime(Foulon1['HeureDebut']).dt.date.min(),
                                style = {"font-size": 14}
                            ),
                            html.Button(
                                'Reset',
                                id = "o2_Reset_Date",
                                n_clicks=0
                            ),
                            html.Br(),
                            html.Br(),
                            html.Label(["Formule"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.Dropdown(
                                id='Formule',
                                options=[{'label': i, 'value': i} for i in Foulon1.Formule.unique()],
                                value=Foulon1.Formule.unique()[1], 
                                clearable = False,
                                style = {'color' : 'black', "background-color": "white"}
                            ),
                            html.Br(),
                            html.Label(["Lot"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.Dropdown(
                                    id='o2_Lot',
                                    options=[{'label': i, 'value': i} for i in Data_Foulon1.NomLot.unique()],
                                    value=Data_Foulon1.NomLot.unique()[0],
                                    clearable = False,
                                    style = {'color' : 'black', "background-color": "white"}
                                ),
                            html.Br(),
                            dcc.Checklist(
                                id = "RawData_Tab2",
                                options=[
                                    {'label': 'Données brutes', 'value': 'Raw'},
                                ],
                                value=[],
                                labelStyle = {'color' : 'white'}
                            ),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br()
                        ]
                    ),
                    
                ]
            ),

            ddk.SidebarCompanion(children = [
                html.Div([
                    html.Div([
                        html.Div([
                            dcc.Graph(id = 'o2_Plot_Ph',
                            config={
                            "displaylogo": False,
                            'modeBarButtonsToRemove': ['lasso2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines', 'resetScale2d']
                            })
                        ], className="twelve columns")
                        
                    ], className = "row"),
                    html.Div([
                        html.Div([
                            dcc.Graph(id = 'o2_Plot_Temp',
                            config={
                            "displaylogo": False,
                            'modeBarButtonsToRemove': ['lasso2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines', 'resetScale2d']
                            })
                        ], className="twelve columns")
                        
                    ], className = "row")
                ], className="eight columns"),
                html.Div([
                    html.Br(),
                    html.Div([
                        html.Div([
                            html.Label(["Détection des périodes"], 
                            style = {'color' : 'black', "display": "block", "text-align": "center", "marginBottom" : "10px", 'fontWeight': 'bold'})
                        ], className = "twelve columns")
                    ], className = "row"),
                    html.Div([
                        html.Div([
                            daq.Indicator(
                                id = 'Dechaulage_Detect',
                                label="Déchaulage",
                                labelPosition="bottom",
                                color="#84e61c",
                                value=True
                            )  
                        ],className="three columns"),
                        html.Div([
                            daq.Indicator(
                                id = 'Pickle_Detect',
                                label="Pickle",
                                labelPosition="bottom",
                                color="#84e61c",
                                value=True
                            )  
                        ],className="three columns"),
                        html.Div([
                            daq.Indicator(
                                id = 'FinPickle_Detect',
                                label="Fin de pickle",
                                labelPosition="bottom",
                                color="#84e61c",
                                value=True
                            )  
                        ],className="three columns"),
                        html.Div([
                            daq.Indicator(
                                id = 'Pretannage_Detect',
                                label="Prétannage",
                                labelPosition="bottom",
                                color="#84e61c",
                                value=True
                            )  
                        ],className="three columns"),
                    ], className = "row"), 
                    html.Br(),
                    html.Div([
                        html.Div([
                            html.Label(["Durées des périodes"], 
                            style = {'color' : 'black', "display": "block", "text-align": "center", "marginBottom" : "10px", 'fontWeight': 'bold'})
                        ], className = "twelve columns")
                    ], className = "row"),
                    html.Div([
                            html.Div([
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'gauge_o2_dechaulage',
                                    showCurrentValue=True,
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    value=0, label='Déchaulage', max=5, min=0
                                )
                            ],className="six columns"),
                            html.Div([  
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'gauge_o2_pickle',
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    showCurrentValue=True,
                                    value=0, label='Pickle', max=5, min=0)
                            ], className="six columns")
                        ], className = "row"),
                    html.Div([
                            html.Div([
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'gauge_o2_fin_pickle',
                                    showCurrentValue=True,
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    value=0, label='Fin de Pickle', max=5, min=0
                                )
                            ],className="six columns"),
                            html.Div([  
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'gauge_o2_pretannage',
                                    showCurrentValue=True,
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    value=0, label='Préntannage - Tannage', max=5, min=0)
                            ], className="six columns")
                        ], className = "row"),
                        html.Br(),
                        html.Div([
                            html.Div([
                                html.Label(["Carte de contrôle : Points en dehors des limites de surveilance"], 
                                style = {'color' : 'black', "display": "block", "text-align": "center", "marginBottom" : "10px", 'fontWeight': 'bold'})
                            ], className = "twelve columns")
                        ], className = "row"),
                        html.Div([
                            html.Div([
                                dash_table.DataTable(
                                    id='o2_table_CC',
                                    style_cell={'textAlign': 'center',
                                    'width': '30%',
                                    'border': '0px solid blue'},
                                    style_header={'fontWeight': 'bold'},
                                    style_cell_conditional=[
                                        {
                                            'if': {'column_id': c},
                                            'textAlign': 'left'
                                        } for c in ['']
                                    ],
                                )
                            ],className="twelve columns"),
                        ], className = "row")
                ], className="four columns"),
            ])
                
                
        ]),

# ---------------- #
# --- Onglet 3 --- #
# ---------------- #

        dcc.Tab(label='Comparaison de deux lots', style=tab_style, selected_style=tab_selected_style, children=[
            ddk.Sidebar(
                style = {'width' : '20vw', 'background' : '#002060' , 'height' : '85vh'},
                children=[
                    ddk.ControlCard(
                        type='flat',
                        style={'background' : '#002060'},
                        children=[
                            html.Label(["Période"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.DatePickerRange(
                                id='o3_date_range',
                                display_format="DD/MM/YY",
                                min_date_allowed=pd.to_datetime(Foulon1['HeureDebut']).dt.date.min(),
                                max_date_allowed=pd.to_datetime(Foulon1['HeureDebut']).dt.date.max(),
                                initial_visible_month=pd.to_datetime(Foulon1['HeureDebut']).dt.date.max(),
                                with_portal=True,
                                clearable = False,
                                first_day_of_week = 1,
                                end_date = pd.to_datetime(Foulon1['HeureDebut']).dt.date.max(), 
                                start_date = pd.to_datetime(Foulon1['HeureDebut']).dt.date.min(),
                                style = {"font-size": 14}
                            ),
                            html.Button(
                                'Reset',
                                id = "o3_Reset_Date",
                                n_clicks=0
                            ),
                            html.Br(),
                            html.Label(["Formule"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                                dcc.Dropdown(
                                    id='o3_formule',
                                    options=[{'label': i, 'value': i} for i in Foulon1.Formule.unique()],
                                    value=Foulon1.Formule.unique()[1], 
                                    clearable = False,
                                    style = {'color' : 'black', "background-color": "white"}
                                ),
                            html.Br(),
                            html.Label(["Lot de référence :"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.Dropdown(
                                id='03_Lot_Ref',
                                options=[{'label': i, 'value': i} for i in Data_Foulon1.NomLot.unique()],
                                value=Data_Foulon1.NomLot.unique()[0],
                                clearable = False,
                                style = {'color' : 'black', "background-color": "white"}
                            ),
                            html.Br(),
                            html.Label(["Lot analysé :"], style = {'color' : 'white', "display": "block", "text-align": "left"}),
                            dcc.Dropdown(
                                id='03_Lot_Test',
                                options=[{'label': i, 'value': i} for i in Data_Foulon1.NomLot.unique()],
                                value=Data_Foulon1.NomLot.unique()[1],
                                clearable = False,
                                style = {'color' : 'black', "background-color": "white"}
                            ),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br(),
                            html.Br()
                        ]
                    ),
                ]
            ),
            ddk.SidebarCompanion(children = [
                html.Div([
                    html.Div([
                        html.Div([
                            dcc.Graph(id = 'o3_Plot_pH',
                            config={
                            "displaylogo": False,
                            'modeBarButtonsToRemove': ['lasso2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines', 'resetScale2d']
                            })
                        ], className="twelve columns")
                        
                    ], className = "row"),
                    html.Div([
                        html.Div([
                            dcc.Graph(id = 'o3_Plot_Temp',
                            config={
                            "displaylogo": False,
                            'modeBarButtonsToRemove': ['lasso2d', 'hoverClosestCartesian', 'hoverCompareCartesian', 'toggleSpikelines', 'resetScale2d']
                            })
                        ], className="twelve columns")
                        
                    ], className = "row")
                ], className="eight columns"),
                html.Div([
                    html.Br(),
                    html.Div([
                        html.Div([
                            html.Label(["Détection des périodes"], 
                            style = {'color' : 'black', "display": "block", "text-align": "center", "marginBottom" : "10px", 'fontWeight': 'bold'})
                        ], className = "twelve columns")
                    ], className = "row"),
                    html.Div([
                        html.Div([
                            daq.Indicator(
                                id = 'o3_Dechaulage_Detect',
                                label="Déchaulage",
                                labelPosition="bottom",
                                color="#84e61c",
                                value=True
                            )  
                        ],className="three columns"),
                        html.Div([
                            daq.Indicator(
                                id = 'o3_Pickle_Detect',
                                label="Pickle",
                                labelPosition="bottom",
                                color="#84e61c",
                                value=True
                            )  
                        ],className="three columns"),
                        html.Div([
                            daq.Indicator(
                                id = 'o3_FinPickle_Detect',
                                label="Fin de pickle",
                                labelPosition="bottom",
                                color="#84e61c",
                                value=True
                            )  
                        ],className="three columns"),
                        html.Div([
                            daq.Indicator(
                                id = 'o3_Pretannage_Detect',
                                label="Prétannage",
                                labelPosition="bottom",
                                color="#84e61c",
                                value=True
                            )  
                        ],className="three columns"),
                    ], className = "row"), 
                    html.Br(),
                    html.Div([
                        html.Div([
                            html.Label(["Durées des périodes"], 
                            style = {'color' : 'black', "display": "block", "text-align": "center", "marginBottom" : "10px", 'fontWeight': 'bold'})
                        ], className = "twelve columns")
                    ], className = "row"),
                    html.Div([
                            html.Div([
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'o3_gauge_dechaulage',
                                    showCurrentValue=True,
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    value=0, label='Déchaulage', max=5, min=0
                                )
                            ],className="six columns"),
                            html.Div([  
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'o3_gauge_pickle',
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    showCurrentValue=True,
                                    value=0, label='Pickle', max=5, min=0)
                            ], className="six columns")
                        ], className = "row"),
                    html.Div([
                            html.Div([
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'o3_gauge_fin_pickle',
                                    showCurrentValue=True,
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    value=0, label='Fin de Pickle', max=5, min=0
                                )
                            ],className="six columns"),
                            html.Div([  
                                daq.Gauge(style={'margin-bottom': -70},
                                    id = 'o3_gauge_pretannage',
                                    showCurrentValue=True,
                                    units="Heures",
                                    labelPosition="bottom",
                                    size=160,
                                    value=0, label='Préntannage - Tannage', max=5, min=0)
                            ], className="six columns")
                        ], className = "row"),
                        html.Br(),
                        html.Div([
                            html.Div([
                                html.Label(["Carte de contrôle : Points en dehors des limites de surveilance"], 
                                style = {'color' : 'black', "display": "block", "text-align": "center", "marginBottom" : "10px", 'fontWeight': 'bold'})
                            ], className = "twelve columns")
                        ], className = "row"),
                        html.Div([
                            html.Div([
                                dash_table.DataTable(
                                    id='o3_table_CC',
                                    merge_duplicate_headers=True,
                                    style_cell={'textAlign': 'center',
                                    'width': '16%',
                                    'border': '0px solid blue'},
                                    style_header={'fontWeight': 'bold'},
                                    style_cell_conditional=[
                                        {
                                            'if': {'column_id': c},
                                            'textAlign': 'left',
                                            'width': '30%'
                                        } for c in ['']
                                    ],
                                )
                            ],className="twelve columns"),
                        ], className = "row")
                ], className="four columns"),

            ],),
        ]),

# ---------------- #
# --- Onglet 4 --- #
# ---------------- #

        dcc.Tab(label='Base des alertes', style=tab_style, selected_style=tab_selected_style, children=[
            dash_table.DataTable(
                columns=[
                    {"name": ["Lot", "Formule"], "id": "Formule"},
                    {"name": ["Lot", "ID"], "id": "NomLot"},
                    {"name": ["Détection Période", "Dechaulage"], "id": "Dechaulage_detect_icon"},
                    {"name": ["Détection Période", "Pickle"], "id": "Pickle_detect_icon"},
                    {"name": ["Détection Période", "Fin Pickle"], "id": "Fin_Pickle_detect_icon"},
                    {"name": ["Détection Période", "Pretannage Tannage"], "id": "Pretannage_Tannage_detect_icon"},
                    {"name": ["Durée", "Dechaulage"], "id": "Alerte_Duree_Dechaulage_icon"},
                    {"name": ["Durée", "Pickle"], "id": "Alerte_Duree_Pickle_icon"},
                    {"name": ["Durée", "Fin Pickle"], "id": "Alerte_Duree_Fin_Pickle_icon"},
                    {"name": ["Durée", "Pretannage Tannage"], "id": "Alerte_Duree_Pretannage_Tannage_icon"},
                    {"name": ["Points en dehors des limites : pH", "Dechaulage"], "id": "Dechaulage_Ph_out_pc"},
                    {"name": ["Points en dehors des limites : pH", "Pickle"], "id": "Pickle_Ph_out_pc"},
                    {"name": ["Points en dehors des limites : pH", "Fin Pickle"], "id": "Fin_Pickle_Ph_out_pc"},
                    {"name": ["Points en dehors des limites : pH", "Pretannage Tannage"], "id": "Pretannage_Tannage_Ph_out_pc"},
                    {"name": ["Points en dehors des limites : Température", "Dechaulage"], "id": "Dechaulage_Temp_out_pc"},
                    {"name": ["Points en dehors des limites : Température", "Pickle"], "id": "Pickle_Temp_out_pc"},
                    {"name": ["Points en dehors des limites : Température", "Fin Pickle"], "id": "Fin_Pickle_Temp_out_pc"},
                    {"name": ["Points en dehors des limites : Température", "Pretannage Tannage"], "id": "Pretannage_Tannage_Temp_out_pc"},
                ],
                data=Alertes.to_dict('records'),
                merge_duplicate_headers=True,
                #sort_action='native',
                filter_action='native',
                #fixed_rows={'headers': True},
                style_data_conditional=[
                    {
                    'if': {
                    'filter_query': '{{{col}}} > 50'.format(col=col),
                    'column_id': col + "_pc"
                    },
                    'color': 'rgba(228,56,35,1)'
                    } for col in cols_pc
                ] +
                [
                    {
                    'if': {
                    'filter_query': '{{{col}}} = 0'.format(col=col),
                    'column_id': col + "_pc"
                    },
                    'color': 'rgba(132, 230, 28,1)'
                    } for col in cols_pc

                ]+
                [
                    {
                    'if': {
                    'filter_query': '{{{col}}} > 0 && {{{col}}} <= 50'.format(col=col),
                    'column_id': col + "_pc"
                    },
                    'color': 'rgba(242, 144, 24,1)'
                    } for col in cols_pc

                ],
                style_header={
                    'backgroundColor': 'rgb(230, 230, 230)',
                    'fontWeight': 'bold',
                    'textAlign': 'center',
                    'height': 'auto',
                    'whiteSpace': 'normal'},
                style_table={
                    'height': 900,
                    'maxHeight' : '76vh',
                    'overflowY': 'scroll',
                    'overflowX': 'auto'
                },
                style_cell={
                    'minWidth': 95,
                    'maxWidth': 100
                }
            )
                    
        ])
    ])
])

# -------------------------------- #
# ----------  Callbacks ---------- #
# -------------------------------- #

# ---------------- #
# --- Onglet 1 --- #
# ---------------- #

@app.callback(
    [Output('o1_Plot_pH', 'figure')],
    [Input('o1_Foulon', 'value')]
)
def update_o1_Plot_pH(o1_Foulon):
    data = Data_Foulon1[(Data_Foulon1.NomLot ==19999) & (Data_Foulon1.Temps <= Data_Foulon1.Temps.max()/8)]
    Lot=data[(data.Ph_sd_curve.isnull()==False) & (data.Period != "autre")].reset_index(drop=True)
    Lot.loc[:,"Temps"]=np.arange(0,Lot.shape[0]*30,30)
    figure = go.Figure()
    figure['layout'] = {'xaxis':{'range':[0,20]},}
    figure.add_trace(go.Scatter(
        x=Lot.Temps/3600,
        y=Lot.Ph_curve,
        mode='markers',
        marker=dict(
            size=2,
            color= 'rgba(26, 118, 189, 1)'
        ),
        hovertemplate = '%{y:.2f}',
        showlegend=False, 
        name = "pH"
        )
    ),
    figure.update_layout(go.Layout(
            yaxis = dict(title = "pH", rangemode = "nonnegative"),
            xaxis = dict(title = "Temps (Heures)", rangemode = "nonnegative"),
            margin=dict(t=20)
    )),
    return [figure]

@app.callback(
    [Output('o1_Plot_Temp', 'figure')],
    [Input('o1_Foulon', 'value')]
)
def update_o1_Plot_Temp(o1_Foulon):
    data = Data_Foulon1[(Data_Foulon1.NomLot ==19999) & (Data_Foulon1.Temps <= Data_Foulon1.Temps.max()/8)]
    Lot=data[(data.Ph_sd_curve.isnull()==False) & (data.Period != "autre")].reset_index(drop=True)
    Lot.loc[:,"Temps"]=np.arange(0,Lot.shape[0]*30,30)
    figure = go.Figure()
    figure['layout'] = {'xaxis':{'range':[0,20]},}
    figure.add_trace(go.Scatter(
        x=Lot.Temps/3600,
        y=Lot.Temp_curve,
        mode='markers',
        marker=dict(
            size=2,
            color= 'rgba(26, 118, 189, 1)'
        ),
        hovertemplate = '%{y:.1f}',
        showlegend=False, 
        name = "Température"
        )
    ),
    figure.update_layout(go.Layout(
            yaxis = dict(title = "pH", rangemode = "nonnegative"),
            xaxis = dict(title = "Temps (Heures)", rangemode = "nonnegative"),
            margin=dict(t=20)
    )),
    return [figure]

# ---------------- #
# --- Onglet 2 --- #
# ---------------- #

@app.callback(
    [Output("o2_table_CC", "columns"),
    Output("o2_table_CC", "data"),
    Output("o2_table_CC", "style_data_conditional")],
    [Input("o2_Lot", "value")]
)
def update_o2_table_CC(lot_name):
    Tab = Alertes[Alertes["NomLot"] == lot_name]
    donnees = np.array([["Déchaulage",Tab.Dechaulage_Ph_out_pc.iloc[0],Tab.Dechaulage_Temp_out_pc.iloc[0], Tab.Dechaulage_Ph_out.iloc[0],Tab.Dechaulage_Temp_out.iloc[0]], 
    ["Pickle",Tab.Pickle_Ph_out_pc.iloc[0],Tab.Pickle_Temp_out_pc.iloc[0], Tab.Pickle_Ph_out.iloc[0],Tab.Pickle_Temp_out.iloc[0]], 
    ["Fin Pickle", Tab.Fin_Pickle_Ph_out_pc.iloc[0],Tab.Fin_Pickle_Temp_out_pc.iloc[0], Tab.Fin_Pickle_Ph_out.iloc[0],Tab.Fin_Pickle_Temp_out.iloc[0]], 
    ["Prétannage - Tannage", Tab.Pretannage_Tannage_Ph_out_pc.iloc[0],Tab.Pretannage_Tannage_Temp_out_pc.iloc[0],Tab.Pretannage_Tannage_Ph_out.iloc[0],Tab.Pretannage_Tannage_Temp_out.iloc[0]]])
    index = ["Déchaulage", "Pickle", "Fin de Pickle", "Prétannage / Tannage"]
    df = pd.DataFrame(donnees,index=index, columns=["","pH_pc", "Température_pc", "pH", "Température"])
    columns=[{"name": i, "id": i} for i in df.columns[0:3]]
    data=df.to_dict('records')
    
    style_data_conditional=(
        [
                    {
                    'if': {
                    'filter_query': '{{{col}}} > 50'.format(col=col),
                    'column_id': col + "_pc"
                    },
                    'color': 'rgba(228,56,35,1)'
                    } for col in df.columns[3:5]
                ] +
                [
                    {
                    'if': {
                    'filter_query': '{{{col}}} = 0'.format(col=col),
                    'column_id': col + "_pc"
                    },
                    'color': 'rgba(132, 230, 28,1)'
                    } for col in df.columns[3:5]

                ]+
                [
                    {
                    'if': {
                    'filter_query': '{{{col}}} > 0 && {{{col}}} <= 50'.format(col=col),
                    'column_id': col + "_pc"
                    },
                    'color': 'rgba(242, 144, 24,1)'
                    } for col in df.columns[3:5]
                ])
    columns[1]["name"] = "pH",
    columns[2]["name"] = "Température",
    return [columns, data, style_data_conditional]

@app.callback(
    [Output('o2_Lot', 'options'),
    Output('o2_Lot', 'value'),
    Output('o2_Lot', 'disabled')],
    [Input('Formule', 'value'),
    Input('o2_date_range','start_date'), 
    Input('o2_date_range', 'end_date')]
)
def update_Lot_liste(formule, start, end):
    liste = Foulon1[(Foulon1.Formule == formule) & 
    (Foulon1.Courbe_exploitable == "OUI") &
    (pd.to_datetime(Foulon1['HeureDebut']).dt.date >= dt.strptime(re.split('T| ', start)[0], '%Y-%m-%d').date()) & 
    (pd.to_datetime(Foulon1['HeureDebut']).dt.date <= dt.strptime(re.split('T| ', end)[0], '%Y-%m-%d').date())]
    if liste.size == 0 :
        Liste = [{'label': "Aucun lot", 'value': Foulon1[(Foulon1.Formule == formule)].NomLot.unique()[0]}]
        Value = Foulon1[(Foulon1.Formule == formule)].NomLot.unique()[0]
        disabled = True
    else :
        Liste = [{'label': i, 'value': i} for i in liste.NomLot.unique()]
        Value = liste.NomLot.unique()[0]
        disabled = False
    return [Liste, Value, disabled]

@app.callback(
    [Output("o2_date_range", "end_date"),
    Output("o2_date_range", "start_date")],
    [Input("o2_Reset_Date", "n_clicks")]
)
def reset_o2_date_range(n_clicks):
    end_date = pd.to_datetime(Foulon1['HeureDebut']).dt.date.max()
    start_date = pd.to_datetime(Foulon1['HeureDebut']).dt.date.min()
    return [end_date, start_date]


@app.callback(
    [Output('Dechaulage_Detect', 'color'),
    Output('Pickle_Detect', 'color'),
    Output('FinPickle_Detect', 'color'),
    Output('Pretannage_Detect', 'color')],
    [Input('o2_Lot', 'value')]
)

def update_Period_Detect_indicators(lot_name):
    Col_Dech = "#84e61c"
    Col_Pickle= "#84e61c"
    Col_FinPickle= "#84e61c"
    Col_Pretannage= "#84e61c"
    if Alertes[Alertes["NomLot"] == lot_name].Dechaulage_detect.bool() == False:
        Col_Dech = "rgba(228,56,35,1)"
    if Alertes[Alertes["NomLot"] == lot_name].Pickle_detect.bool()== False:
        Col_Pickle = "rgba(228,56,35,1)"
    if Alertes[Alertes["NomLot"] == lot_name].Fin_Pickle_detect.bool()== False:
        Col_FinPickle = "rgba(228,56,35,1)"
    if Alertes[Alertes["NomLot"] == lot_name].Pretannage_Tannage_detect.bool()== False:
        Col_Pretannage = "rgba(228,56,35,1)"
    return [Col_Dech, Col_Pickle, Col_FinPickle, Col_Pretannage]

@app.callback(
    [Output('gauge_o2_dechaulage', 'value'),
    Output('gauge_o2_dechaulage', 'color'),
    Output('gauge_o2_dechaulage', 'min'),
    Output('gauge_o2_dechaulage', 'max')],
    [Input('o2_Lot', 'value'),
      Input("RawData_Tab2", "value")]
)
def update_gauge_o2_dechaulage(lot_name, raw):
    duree = Foulon1[Foulon1["NomLot"] == lot_name].Duree_Dechaulage.iloc[0] /3600
    Low = IC_durrees["Dechaulage"][2]/3600
    Up = IC_durrees["Dechaulage"][3]/3600
    Lower = IC_durrees["Dechaulage"][0]/3600
    Upper = IC_durrees["Dechaulage"][1]/3600
    min = 0
    maxi = round(max(1.2*Upper, duree), 1)
    default = "#84e61c"
    if((duree < Lower) | (duree > Upper)):
        default = "rgba(242, 144, 24,1)"
        if((duree < Low) | (duree > Up)):
            default = "rgba(228,56,35,1)"    
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low],"rgba(242, 144, 24,1)": [Low,Lower], "#84e61c":[Lower,Upper],"#f29018": [Upper,Up],"red":[Up,maxi]},
           "default":default}
    return [duree, color, min, maxi]


@app.callback(
    [Output('gauge_o2_pickle', 'value'),
    Output('gauge_o2_pickle', 'color'),
    Output('gauge_o2_pickle', 'min'),
    Output('gauge_o2_pickle', 'max')],
    [Input('o2_Lot', 'value'),
      Input("RawData_Tab2", "value")]
)
def update_gauge_o2_pickle(lot_name, raw):
    duree = Foulon1[Foulon1["NomLot"] == lot_name].Duree_Pickle.iloc[0] /3600
    Low = IC_durrees["Pickle"][2]/3600
    Up = IC_durrees["Pickle"][3]/3600
    Lower = IC_durrees["Pickle"][0]/3600
    Upper = IC_durrees["Pickle"][1]/3600
    min = 0#round(0.8*Lower, 1)
    maxi = round(max(1.2*Upper, duree), 1)
    default = "#84e61c"
    if((duree < Lower) | (duree > Upper)):
        default = "rgba(242, 144, 24,1)"
        if((duree < Low) | (duree > Up)):
            default = "rgba(228,56,35,1)"
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low],"rgba(242, 144, 24,1)": [Low,Lower],"#84e61c":[Lower,Upper],"#f29018": [Upper,Up],"red":[Up,maxi]},
           "default":default}
    return [duree, color, min, maxi]

@app.callback(
    [Output('gauge_o2_fin_pickle', 'value'),
    Output('gauge_o2_fin_pickle', 'color'),
    Output('gauge_o2_fin_pickle', 'min'),
    Output('gauge_o2_fin_pickle', 'max')],
    [Input('o2_Lot', 'value'),
      Input("RawData_Tab2", "value")]
)
def update_gauge_o2_fin_pickle(lot_name, raw):
    duree = Foulon1[Foulon1["NomLot"] == lot_name].Duree_Fin_Pickle.iloc[0] /3600
    Low = IC_durrees["Fin_Pickle"][2]/3600
    Up = IC_durrees["Fin_Pickle"][3]/3600
    Lower = IC_durrees["Fin_Pickle"][0]/3600
    Upper = IC_durrees["Fin_Pickle"][1]/3600
    min = 0#round(0.8*Lower, 1)
    maxi = round(max(1.2*Upper, duree), 1)
    default = "#84e61c"
    if((duree < Lower) | (duree > Upper)):
        default = "rgba(242, 144, 24,1)"
        if((duree < Low) | (duree > Up)):
            default = "rgba(228,56,35,1)"
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low],"rgba(242, 144, 24,1)": [Low,Lower], "#84e61c":[Lower,Upper],"#f29018": [Upper,Up],"red":[Up,maxi]},
           "default":default}
    return [duree, color, min, maxi]

@app.callback(
    [Output('gauge_o2_pretannage', 'value'),
    Output('gauge_o2_pretannage', 'color'),
    Output('gauge_o2_pretannage', 'min'),
    Output('gauge_o2_pretannage', 'max')],
    [Input('o2_Lot', 'value'),
      Input("RawData_Tab2", "value")]
)
def update_gauge_o2_pretannage(lot_name, raw):
    duree = Foulon1[Foulon1["NomLot"] == lot_name].Duree_Pretannage_Tannage.iloc[0] /3600
    Low = IC_durrees["Pretannage_Tannage"][2]/3600
    Up = IC_durrees["Pretannage_Tannage"][3]/3600
    Lower = IC_durrees["Pretannage_Tannage"][0]/3600
    Upper = IC_durrees["Pretannage_Tannage"][1]/3600
    min = 0#round(0.8*Lower, 1)
    maxi = round(max(1.2*Upper, duree), 1)
    default = "#84e61c"
    if((duree < Lower) | (duree > Upper)):
        default = "rgba(242, 144, 24,1)"
        if((duree < Low) | (duree > Up)):
            default = "rgba(228,56,35,1)"
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low], 
    "rgba(242, 144, 24,1)": [Low,Lower], 
    "#84e61c":[Lower,Upper],
    "#f29018": [Upper,Up], 
    "red":[Up,maxi]},
           "default":default}
    return [duree, color, min, maxi]

@app.callback(
    [Output('o2_Plot_Ph', 'figure')],
    [Input('o2_Lot', 'value'),
      Input("RawData_Tab2", "value"),
      Input('o2_Plot_Temp', 'relayoutData')]
)
def update_o2_Plot_Ph(lot_name, raw, select_data):
    data = Data_Foulon1[Data_Foulon1["NomLot"]==lot_name]
    Lot=data[(data.Ph_sd_curve.isnull()==False) & (data.Period != "autre")].reset_index(drop=True)

    try:
        res = segDTW(Lot,cc)
        x=np.concatenate((res[0], 
                max(res[0])+res[1] + 1,
                max(res[0])+max(res[1])+res[2] + 1,
                max(res[0])+max(res[1])+max(res[2])+res[3] + 1))
    except ValueError:
        print("Ca marche pas, donc je fais sur tout le signal et il faudra l'écrire quelquepart")
        x = allDTW(Lot,cc)
    cc_recal=cc.loc[x].reset_index(drop=True)
    cc_recal.Temps=Lot.Temps

    LotMerged = pd.merge(Lot, cc_recal, on = "Temps", how = "left")
    LotMerged["pH_Out"] = ((LotMerged.Ph_curve_x > LotMerged.lim_Ph_sup) | (LotMerged.Ph_curve_x < LotMerged.lim_Ph_inf)).astype('int')
    LotMerged = LotMerged.append(pd.Series(dtype="float64"), ignore_index=True)
    LotMerged.iloc[-1, LotMerged.columns.get_loc('pH_Out')] = 1

    figure1 = go.Figure()
    try:
        figure1['layout'] = {'xaxis':{'range':[select_data['xaxis.range[0]'],select_data['xaxis.range[1]']]},
    }
    except:
        figure1['layout'] = {'xaxis':{'range':[min(Lot["Temps"]/3600),max(Lot["Temps"]/3600)]}}
    figure1.add_trace(go.Scatter(x=cc_recal.Temps/3600,
        y=cc_recal.lim_Ph_sup,
        fill=None,
        mode='lines',
        marker_color='#919191', 
        showlegend=False, 
        hovertemplate = '%{y:.2f}',
        name = "Limite Sup.")),
    figure1.add_trace(go.Scatter(x=cc_recal.Temps/3600,
        y=cc_recal.lim_Ph_inf,
        mode='lines',
        fill='tonexty',
        marker_color='#919191', 
        showlegend=False, 
        hovertemplate = '%{y:.2f}',
        name = "Limite Inf.")),
    figure1.add_trace(go.Scatter(
        x=LotMerged.Temps/3600,
        y=LotMerged.Ph_curve_x,
        mode='markers',
        marker=dict(
            size=2,
            color=LotMerged["pH_Out"],
            colorscale=[[0, 'rgba(26, 118, 189, 1)'], [1, 'red']]
        ),
        hovertemplate = '%{y:.2f}',
        showlegend=False, 
        name = "pH"
        )
    ),
    if raw==["Raw"]:
        figure1.add_trace(go.Scatter(
            x=Lot["Temps"]/3600,
            y=Lot["Ph_inter"],
            mode='markers',
            marker_color='rgba(173, 173, 173, .5)', 
            hovertemplate = '%{y:.2f}',
            showlegend=False
            )
        )
    if "Dechaulage" in Lot.Period.unique():
        figure1.add_shape(
            type="line",
            x0=Lot[Lot.Period=="Dechaulage"].Temps.max()/3600,
            x1=Lot[Lot.Period=="Dechaulage"].Temps.max()/3600,
            y0 = 0,
            y1 = cc_recal.lim_Ph_sup.max(),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot"
            )
        )
    if "Pickle" in Lot.Period.unique():
        figure1.add_shape(
            type="line",
            x0=Lot[Lot.Period=="Pickle"].Temps.max()/3600,
            x1=Lot[Lot.Period=="Pickle"].Temps.max()/3600,
            y0 = 0,
            y1 = cc_recal.lim_Ph_sup.max(),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            )
        )
    if "Fin_Pickle" in Lot.Period.unique():
        figure1.add_shape(
            type="line",
            x0=Lot[Lot.Period=="Fin_Pickle"].Temps.max()/3600,
            x1=Lot[Lot.Period=="Fin_Pickle"].Temps.max()/3600,
            y0 = 0,
            y1 = cc_recal.lim_Ph_sup.max(),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            )
        )
    if "Pretannage" in Lot.Period.unique():
        figure1.add_shape(
            type="line",
            x0=Lot[Lot.Period=="Pretannage"].Temps.max()/3600,
            x1=Lot[Lot.Period=="Pretannage"].Temps.max()/3600,
            y0 = 0,
            y1 = cc_recal.lim_Ph_sup.max(),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            )
        )
    figure1.update_layout(go.Layout(
            yaxis = dict(title = "pH", rangemode = "nonnegative"),
            xaxis = dict(title = "Temps (Heures)", rangemode = "nonnegative"),
            margin=dict(t=20)
    )),
    return [figure1]


@app.callback(
    [Output('o2_Plot_Temp', 'figure')],
    [Input('o2_Lot', 'value'),
      Input("RawData_Tab2", "value"),
      Input('o2_Plot_Ph', 'relayoutData')],
)
def update_o2_Plot_Temp(lot_name, raw, select_data):
    data = Data_Foulon1[Data_Foulon1["NomLot"]==lot_name]
    Lot=data[(data.Ph_sd_curve.isnull()==False) & (data.Period != "autre")].reset_index(drop=True)
    try:
        res = segDTW(Lot, cc)
        x=np.concatenate((res[0], 
                max(res[0])+res[1] + 1,
                max(res[0])+max(res[1])+res[2] + 1,
                max(res[0])+max(res[1])+max(res[2])+res[3] + 1))
    except ValueError:
        print("Ca marche pas, donc je fais sur tout le signal et il faudra l'écrire quelquepart")
        x = allDTW(Lot, cc)
    cc_recal=cc.loc[x].reset_index(drop=True)
    cc_recal.Temps=Lot.Temps

    LotMerged = pd.merge(Lot, cc_recal, on = "Temps", how = "left")
    LotMerged["Temp_Out"] = ((LotMerged.Temp_curve_x > LotMerged.lim_Temp_sup) | (LotMerged.Temp_curve_x < LotMerged.lim_Temp_inf)).astype('int')
    LotMerged = LotMerged.append(pd.Series(dtype="float64"), ignore_index=True)
    LotMerged.iloc[-1, LotMerged.columns.get_loc('Temp_Out')] = 1

    figure = go.Figure()
    try:
        figure['layout'] = {'xaxis':{'range':[select_data['xaxis.range[0]'],select_data['xaxis.range[1]']]},
    }
    except:
        figure['layout'] = {'xaxis':{'range':[min(Lot["Temps"]/3600),max(Lot["Temps"]/3600)]}}
    figure.add_trace(go.Scatter(x=cc_recal.Temps/3600,
                y=cc_recal.lim_Temp_sup,
                fill=None,
                mode='lines',
                marker_color='#919191', 
                showlegend=False, 
                hovertemplate = '%{y:.1f}',
                name = "Limite Sup.")),
    figure.add_trace(go.Scatter(x=cc_recal.Temps/3600,
                y=cc_recal.lim_Temp_inf,
                mode='lines',
                fill='tonexty',
                marker_color='#919191', 
                showlegend=False, 
                hovertemplate = '%{y:.1f}',
                name = "Limite Inf.")),
    figure.add_trace(go.Scatter(
            x=LotMerged.Temps/3600,
            y=LotMerged.Temp_curve_x,
            mode='markers',
            marker=dict(
                size=2,
                color=LotMerged["Temp_Out"],
                colorscale=[[0, 'rgba(26, 118, 189, 1)'], [1, 'red']]
            ),
            hovertemplate = '%{y:.1f}',
            showlegend=False, 
            name = "Température"
            )),
    if raw==["Raw"]:
        figure.add_trace(go.Scatter(
            x=Lot["Temps"]/3600,
            y=Lot["Temp_inter"],
            mode='markers',
            marker_color='rgba(173, 173, 173, .5)', 
            hovertemplate = '%{y:.1f}',
            showlegend=False
            )
        )
    
    if "Dechaulage" in Lot.Period.unique():
        figure.add_shape(
            type="line",
            x0=Lot[Lot.Period=="Dechaulage"].Temps.max()/3600,
            x1=Lot[Lot.Period=="Dechaulage"].Temps.max()/3600,
            y0 = cc_recal.lim_Temp_inf.min(),
            y1 = cc_recal.lim_Temp_sup.max(),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            ),
        ), 
    if "Pickle" in Lot.Period.unique():
        figure.add_shape(
            type="line",
            x0=Lot[Lot.Period=="Pickle"].Temps.max()/3600,
            x1=Lot[Lot.Period=="Pickle"].Temps.max()/3600,
            y0 = cc_recal.lim_Temp_inf.min(),
            y1 = cc_recal.lim_Temp_sup.max(),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            ),
        ), 
    if "Fin_Pickle" in Lot.Period.unique():
        figure.add_shape(
            type="line",
            x0=Lot[Lot.Period=="Fin_Pickle"].Temps.max()/3600,
            x1=Lot[Lot.Period=="Fin_Pickle"].Temps.max()/3600,
            y0 = cc_recal.lim_Temp_inf.min(),
            y1 = cc_recal.lim_Temp_sup.max(),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            ),
        ), 
    if "Pretannage" in Lot.Period.unique():
        figure.add_shape(
            type="line",
            x0=Lot[Lot.Period=="Pretannage"].Temps.max()/3600,
            x1=Lot[Lot.Period=="Pretannage"].Temps.max()/3600,
            y0 = cc_recal.lim_Temp_inf.min(),
            y1 = cc_recal.lim_Temp_sup.max(),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            ),
        ), 
    figure.update_layout(go.Layout(
            yaxis = dict(title = "Température (°C)", rangemode = "nonnegative"),
            xaxis = dict(title = "Temps (Heures)", rangemode = "nonnegative"),
            margin=dict(t=20)
    )),
    return [figure]

# ---------------- #
# --- Onglet 3 --- #
# ---------------- #

@app.callback(
    [Output('03_Lot_Ref', 'options'),
    Output('03_Lot_Ref', 'value'),
    Output('03_Lot_Ref', 'disabled')],
    [Input('o3_formule', 'value'),
    Input('o3_date_range','start_date'), 
    Input('o3_date_range', 'end_date')]
)
def update_03_Lot_Ref(formule, start, end):
    liste = Foulon1[(Foulon1.Formule == formule) & 
    (Foulon1.Courbe_exploitable == "OUI") &
    (pd.to_datetime(Foulon1['HeureDebut']).dt.date >= dt.strptime(re.split('T| ', start)[0], '%Y-%m-%d').date()) & 
    (pd.to_datetime(Foulon1['HeureDebut']).dt.date <= dt.strptime(re.split('T| ', end)[0], '%Y-%m-%d').date())]
    if liste.size == 0 :
        Liste = [{'label': "Aucun lot", 'value': Foulon1[(Foulon1.Formule == formule)].NomLot.unique()[0]}]
        Value = Foulon1[(Foulon1.Formule == formule)].NomLot.unique()[0]
        disabled=True
    else :
        Liste = [{'label': i, 'value': i} for i in liste.NomLot.unique()]
        Value = liste.NomLot.unique()[0]
        disabled=False
    return [Liste, Value, disabled]

@app.callback(
    [Output('03_Lot_Test', 'options'),
    Output('03_Lot_Test', 'value'),
    Output('03_Lot_Test', 'disabled')],
    [Input('o3_formule', 'value'),
    Input('o3_date_range','start_date'), 
    Input('o3_date_range', 'end_date'),
    Input('03_Lot_Ref', 'value')]
)
def update_03_Lot_Test(formule, start, end, lotRef):
    liste = Foulon1[(Foulon1.Formule == formule) & 
    (Foulon1.Courbe_exploitable == "OUI") &
    (Foulon1.NomLot != lotRef) &
    (pd.to_datetime(Foulon1['HeureDebut']).dt.date >= dt.strptime(re.split('T| ', start)[0], '%Y-%m-%d').date()) & 
    (pd.to_datetime(Foulon1['HeureDebut']).dt.date <= dt.strptime(re.split('T| ', end)[0], '%Y-%m-%d').date())]
    if liste.size == 0 :
        Liste = [{'label': "Aucun lot", 'value': Foulon1[(Foulon1.Formule == formule)].NomLot.unique()[0]}]
        Value = Foulon1[(Foulon1.Formule == formule)].NomLot.unique()[0]
        disabled=True
    else :
        Liste = [{'label': i, 'value': i} for i in liste.NomLot.unique()]
        Value = liste.NomLot.unique()[0]
        disabled = False
    return [Liste, Value, disabled]

@app.callback(
    [Output("o3_date_range", "end_date"),
    Output("o3_date_range", "start_date")],
    [Input("o3_Reset_Date", "n_clicks")]
)
def reset_o3_date_range(n_clicks):
    end_date = pd.to_datetime(Foulon1['HeureDebut']).dt.date.max()
    start_date = pd.to_datetime(Foulon1['HeureDebut']).dt.date.min()
    return [end_date, start_date]

@app.callback(
    [Output('o3_Plot_pH', 'figure')],
    [Input('03_Lot_Ref', 'value'),
      Input('03_Lot_Test', 'value'),
      Input('o3_Plot_Temp', 'relayoutData')]
)

def update_o3_Plot_pH(LotRef, LotTest, select_data):
    data = Data_Foulon1[(Data_Foulon1.Ph_sd_curve.isnull() == False) & (Data_Foulon1.Period != "autre")].reset_index(drop=True)
    data_lot1 = data[data["NomLot"]==LotRef].reset_index(drop=True)
    data_lot2 = data[data["NomLot"]==LotTest].reset_index(drop=True)
    try:
        res = segDTW(data_lot2,data_lot1, index_reference = False)
        x = np.concatenate((res[0],
                            max(res[0]) + res[1] + 1,
                            max(res[0]) + max(res[1]) + res[2] + 1,
                            max(res[0]) + max(res[1]) + max(res[2]) + res[3] + 1))
    except ValueError:
        print("Ca marche pas, donc je fais sur tout le signal et il faudra l'écrire quelquepart")
        x = allDTW(data_lot2,data_lot1, index_reference = False)

    data_lot2_recal=data_lot2.loc[x].reset_index(drop = True)
    data_lot2_recal.Temps = data_lot1.Temps
    

    try:
        res = segDTW(data_lot1, cc)
        x=np.concatenate((res[0], 
                max(res[0])+res[1] + 1,
                max(res[0])+max(res[1])+res[2] + 1,
                max(res[0])+max(res[1])+max(res[2])+res[3] + 1))
    except ValueError:
        print("Ca marche pas, donc je fais sur tout le signal et il faudra l'écrire quelquepart")
        x = allDTW(data_lot1, cc)
    cc_recal=cc.loc[x].reset_index(drop = True)
    cc_recal.Temps = data_lot1.Temps

    figure = go.Figure()
    figure.add_trace(go.Scatter(x=cc_recal.Temps/3600,
        y=cc_recal.lim_Ph_sup,
        fill=None,
        mode='lines',
        marker_color='#919191', 
        showlegend=False, 
        hovertemplate = '%{y:.2f}',
        name = "Limite Sup.")),
    figure.add_trace(go.Scatter(x=cc_recal.Temps/3600,
        y=cc_recal.lim_Ph_inf,
        mode='lines',
        fill='tonexty',
        marker_color='#919191', 
        showlegend=False, 
        hovertemplate = '%{y:.2f}',
        name = "Limite Inf.")),
    figure.add_trace(go.Scatter(
        x=data_lot1["Temps"]/3600,
        y=data_lot1["Ph_curve"],
        mode='lines',
        hovertemplate = '%{y:.2f}',
        name = "Lot Référence",
        marker_color='rgba(252, 17, 0, 1)', 
        showlegend=True)),
    figure.add_trace(go.Scatter(
        x=data_lot2_recal["Temps"]/3600,
        y=data_lot2_recal["Ph_curve"], 
        mode="lines",
        hovertemplate = '%{y:.2f}',
        name = "Lot Analysé",
        marker_color = 'rgba(26, 118, 189, 1)',
        showlegend=True)),
    figure.update_layout(go.Layout(
        yaxis = dict(title = "pH", rangemode = "nonnegative"),
        xaxis = dict(title = "Temps (Heures)", rangemode = "nonnegative"))),
    try:
        figure['layout'] = {'xaxis':{'range':[select_data['xaxis.range[0]'],select_data['xaxis.range[1]']]},
    }
    except:
        figure['layout'] = {'xaxis':{'range':[min(data_lot1["Temps"]/3600),max(data_lot1["Temps"]/3600)]}}
    if "Dechaulage" in data_lot1.Period.unique():
        figure.add_shape(
            type="line",
            x0=data_lot1[data_lot1.Period=="Dechaulage"].Temps.max()/3600,
            x1=data_lot1[data_lot1.Period=="Dechaulage"].Temps.max()/3600,
            y0 = 0,
            y1 = max(cc_recal.lim_Ph_sup.max(), data_lot1["Ph_curve"].max(), data_lot2["Ph_curve"].max()),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot"
            )
        )
    if "Pickle" in data_lot1.Period.unique():
        figure.add_shape(
            type="line",
            x0=data_lot1[data_lot1.Period=="Pickle"].Temps.max()/3600,
            x1=data_lot1[data_lot1.Period=="Pickle"].Temps.max()/3600,
            y0 = 0,
            y1 = max(cc_recal.lim_Ph_sup.max(), data_lot1["Ph_curve"].max(), data_lot2["Ph_curve"].max()),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            )
        )
    if "Fin_Pickle" in data_lot1.Period.unique():
        figure.add_shape(
            type="line",
            x0=data_lot1[data_lot1.Period=="Fin_Pickle"].Temps.max()/3600,
            x1=data_lot1[data_lot1.Period=="Fin_Pickle"].Temps.max()/3600,
            y0 = 0,
            y1 = max(cc_recal.lim_Ph_sup.max(), data_lot1["Ph_curve"].max(), data_lot2["Ph_curve"].max()),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            )
        )
    if "Pretannage" in data_lot1.Period.unique():
        figure.add_shape(
            type="line",
            x0=data_lot1[data_lot1.Period=="Pretannage"].Temps.max()/3600,
            x1=data_lot1[data_lot1.Period=="Pretannage"].Temps.max()/3600,
            y0 = 0,
            y1 = max(cc_recal.lim_Ph_sup.max(), data_lot1["Ph_curve"].max(), data_lot2["Ph_curve"].max()),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            )
        )
    figure.update_layout(go.Layout(
            yaxis = dict(title = "pH", rangemode = "nonnegative"),
            xaxis = dict(title = "Temps (Heures)", rangemode = "nonnegative"),
            margin=dict(t=20)
    ))
    
    return [figure]


@app.callback(
    [Output('o3_Plot_Temp', 'figure')],
    [Input('03_Lot_Ref', 'value'),
      Input('03_Lot_Test', 'value'),
      Input('o3_Plot_pH', 'relayoutData')]
)

def update_o3_Plot_Temp(LotRef, LotTest, select_data):
    data = Data_Foulon1[(Data_Foulon1.Ph_sd_curve.isnull() == False) & (Data_Foulon1.Period != "autre")].reset_index(drop=True)
    data_lot1 = data[data["NomLot"]==LotRef].reset_index(drop=True)
    data_lot2 = data[data["NomLot"]==LotTest].reset_index(drop=True)


    try:
        res = segDTW(data_lot2,data_lot1, index_reference = False)
        x = np.concatenate((res[0],
                            max(res[0]) + res[1] + 1,
                            max(res[0]) + max(res[1]) + res[2] + 1,
                            max(res[0]) + max(res[1]) + max(res[2]) + res[3] + 1))
    except ValueError:
        print("Ca marche pas, donc je fais sur tout le signal et il faudra l'écrire quelquepart")
        x = allDTW(data_lot2,data_lot1, index_reference = False)

    data_lot2_recal=data_lot2.loc[x].reset_index(drop = True)
    data_lot2_recal.Temps = data_lot1.Temps
    
    try:
        res = segDTW(data_lot1, cc)
        x=np.concatenate((res[0], 
                max(res[0])+res[1] + 1,
                max(res[0])+max(res[1])+res[2] + 1,
                max(res[0])+max(res[1])+max(res[2])+res[3] + 1))
    except ValueError:
        print("Ca marche pas, donc je fais sur tout le signal et il faudra l'écrire quelquepart")
        x = allDTW(data_lot1, cc)
    cc_recal=cc.loc[x].reset_index(drop = True)
    cc_recal.Temps=data_lot1.Temps

    figure = go.Figure()
    figure.add_trace(go.Scatter(x=cc_recal.Temps/3600,
        y=cc_recal.lim_Temp_sup,
        fill=None,
        mode='lines',
        marker_color='#919191', 
        showlegend=False, 
        hovertemplate = '%{y:.2f}',
        name = "Limite Sup.")),
    figure.add_trace(go.Scatter(x=cc_recal.Temps/3600,
        y=cc_recal.lim_Temp_inf,
        mode='lines',
        fill='tonexty',
        marker_color='#919191', 
        showlegend=False, 
        hovertemplate = '%{y:.2f}',
        name = "Limite Inf.")),
    figure.add_trace(go.Scatter(
        x=data_lot1["Temps"]/3600,
        y=data_lot1["Temp_curve"],
        mode='lines',
        hovertemplate = '%{y:.1f}',
        marker_color='rgba(252, 17, 0, 1)', 
        name = "Lot Référence",
        showlegend=True)),
    figure.add_trace(go.Scatter(
        x=data_lot2_recal["Temps"]/3600,
        y=data_lot2_recal["Temp_curve"], 
        mode="lines",
        hovertemplate = '%{y:.1f}',
        marker_color = 'rgba(26, 118, 189, 1)',
        name = "Lot Analysé",
        showlegend=True)),
    figure.update_layout(go.Layout(
        yaxis = dict(title = "Température (°C)", rangemode = "nonnegative"),
        xaxis = dict(title = "Temps (Heures)", rangemode = "nonnegative"))),

    try:
        figure['layout'] = {'xaxis':{'range':[select_data['xaxis.range[0]'],select_data['xaxis.range[1]']]},
    }
    except:
        figure['layout'] = {'xaxis':{'range':[min(data_lot1["Temps"]/3600),max(data_lot1["Temps"]/3600)]}}
    if "Dechaulage" in data_lot1.Period.unique():
        figure.add_shape(
            type="line",
            x0=data_lot1[data_lot1.Period=="Dechaulage"].Temps.max()/3600,
            x1=data_lot1[data_lot1.Period=="Dechaulage"].Temps.max()/3600,
            y0 = min(cc_recal.lim_Temp_inf.min(), data_lot1["Temp_curve"].min(), data_lot2["Temp_curve"].min()),
            y1 = max(cc_recal.lim_Temp_sup.max(), data_lot1["Temp_curve"].max(), data_lot2["Temp_curve"].max()),

            line=dict(
                color="#595959",
                width=2,
                dash="dashdot"
            )
        )
    if "Pickle" in data_lot1.Period.unique():
        figure.add_shape(
            type="line",
            x0=data_lot1[data_lot1.Period=="Pickle"].Temps.max()/3600,
            x1=data_lot1[data_lot1.Period=="Pickle"].Temps.max()/3600,
            y0 = min(cc_recal.lim_Temp_inf.min(), data_lot1["Temp_curve"].min(), data_lot2["Temp_curve"].min()),
            y1 = max(cc_recal.lim_Temp_sup.max(), data_lot1["Temp_curve"].max(), data_lot2["Temp_curve"].max()),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            )
        )
    if "Fin_Pickle" in data_lot1.Period.unique():
        figure.add_shape(
            type="line",
            x0=data_lot1[data_lot1.Period=="Fin_Pickle"].Temps.max()/3600,
            x1=data_lot1[data_lot1.Period=="Fin_Pickle"].Temps.max()/3600,
            y0 = min(cc_recal.lim_Temp_inf.min(), data_lot1["Temp_curve"].min(), data_lot2["Temp_curve"].min()),
            y1 = max(cc_recal.lim_Temp_sup.max(), data_lot1["Temp_curve"].max(), data_lot2["Temp_curve"].max()),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            )
        )
    if "Pretannage" in data_lot1.Period.unique():
        figure.add_shape(
            type="line",
            x0=data_lot1[data_lot1.Period=="Pretannage"].Temps.max()/3600,
            x1=data_lot1[data_lot1.Period=="Pretannage"].Temps.max()/3600,
            y0 = min(cc_recal.lim_Temp_inf.min(), data_lot1["Temp_curve"].min(), data_lot2["Temp_curve"].min()),
            y1 = max(cc_recal.lim_Temp_sup.max(), data_lot1["Temp_curve"].max(), data_lot2["Temp_curve"].max()),
            line=dict(
                color="#595959",
                width=2,
                dash="dashdot",
            )
        )
    figure.update_layout(go.Layout(
        yaxis = dict(title = "Température (°C)", rangemode = "nonnegative"),
        xaxis = dict(title = "Temps (Heures)", rangemode = "nonnegative"),
        margin=dict(t=20)
    ))
    
    
    return [figure]


@app.callback(
    [Output('o3_gauge_dechaulage', 'value'),
    Output('o3_gauge_dechaulage', 'color'),
    Output('o3_gauge_dechaulage', 'min'),
    Output('o3_gauge_dechaulage', 'max'),
    Output('o3_gauge_dechaulage', 'label')],
    [Input('03_Lot_Ref', 'value'),
    Input('03_Lot_Test', 'value')]
)
def update_gauge_o3_dechaulage(lot_ref_name, lot_test_name):
    duree = Foulon1[Foulon1["NomLot"] == lot_test_name].Duree_Dechaulage.iloc[0] /3600
    duree_ref = (Foulon1[Foulon1["NomLot"] == lot_ref_name].Duree_Dechaulage.iloc[0]/3600).round(1)
    Low = IC_durrees["Dechaulage"][2]/3600
    Up = IC_durrees["Dechaulage"][3]/3600
    Lower = IC_durrees["Dechaulage"][0]/3600
    Upper = IC_durrees["Dechaulage"][1]/3600
    min = 0
    maxi = round(max(1.2*Upper, duree), 1)
    default = "#84e61c"
    if((duree < Lower) | (duree > Upper)):
        default = "rgba(242, 144, 24,1)"
        if((duree < Low) | (duree > Up)):
            default = "rgba(228,56,35,1)"    
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low],"rgba(242, 144, 24,1)": [Low,Lower], "#84e61c":[Lower,Upper],"#f29018": [Upper,Up],"red":[Up,maxi]},
           "default":default}
    label = "Dechaulage (Ref = {}h)".format(duree_ref)
    return [duree, color, min, maxi, label]

@app.callback(
    [Output('o3_gauge_pickle', 'value'),
    Output('o3_gauge_pickle', 'color'),
    Output('o3_gauge_pickle', 'min'),
    Output('o3_gauge_pickle', 'max'),
    Output('o3_gauge_pickle', 'label')],
    [Input('03_Lot_Ref', 'value'),
    Input('03_Lot_Test', 'value')]
)
def update_gauge_o3_pickle(lot_ref_name, lot_test_name):
    duree = Foulon1[Foulon1["NomLot"] == lot_test_name].Duree_Pickle.iloc[0] /3600
    duree_ref = (Foulon1[Foulon1["NomLot"] == lot_ref_name].Duree_Pickle.iloc[0]/3600).round(1)
    Low = IC_durrees["Pickle"][2]/3600
    Up = IC_durrees["Pickle"][3]/3600
    Lower = IC_durrees["Pickle"][0]/3600
    Upper = IC_durrees["Pickle"][1]/3600
    min = 0
    maxi = round(max(1.2*Upper, duree), 1)
    default = "#84e61c"
    if((duree < Lower) | (duree > Upper)):
        default = "rgba(242, 144, 24,1)"
        if((duree < Low) | (duree > Up)):
            default = "rgba(228,56,35,1)"    
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low],"rgba(242, 144, 24,1)": [Low,Lower], "#84e61c":[Lower,Upper],"#f29018": [Upper,Up],"red":[Up,maxi]},
           "default":default}
    label = "Pickle (Ref = {}h)".format(duree_ref)
    return [duree, color, min, maxi, label]

@app.callback(
    [Output('o3_gauge_fin_pickle', 'value'),
    Output('o3_gauge_fin_pickle', 'color'),
    Output('o3_gauge_fin_pickle', 'min'),
    Output('o3_gauge_fin_pickle', 'max'),
    Output('o3_gauge_fin_pickle', 'label')],
    [Input('03_Lot_Ref', 'value'),
    Input('03_Lot_Test', 'value')]
)
def update_o3_gauge_fin_pickle(lot_ref_name, lot_test_name):
    duree = Foulon1[Foulon1["NomLot"] == lot_test_name].Duree_Fin_Pickle.iloc[0] /3600
    duree_ref = (Foulon1[Foulon1["NomLot"] == lot_ref_name].Duree_Fin_Pickle.iloc[0]/3600).round(1)
    Low = IC_durrees["Fin_Pickle"][2]/3600
    Up = IC_durrees["Fin_Pickle"][3]/3600
    Lower = IC_durrees["Fin_Pickle"][0]/3600
    Upper = IC_durrees["Fin_Pickle"][1]/3600
    min = 0
    maxi = round(max(1.2*Upper, duree), 1)
    default = "#84e61c"
    if((duree < Lower) | (duree > Upper)):
        default = "rgba(242, 144, 24,1)"
        if((duree < Low) | (duree > Up)):
            default = "rgba(228,56,35,1)"    
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low],"rgba(242, 144, 24,1)": [Low,Lower], "#84e61c":[Lower,Upper],"#f29018": [Upper,Up],"red":[Up,maxi]},
           "default":default}
    label = "Fin Pickle (Ref = {}h)".format(duree_ref)
    return [duree, color, min, maxi, label]

@app.callback(
    [Output('o3_gauge_pretannage', 'value'),
    Output('o3_gauge_pretannage', 'color'),
    Output('o3_gauge_pretannage', 'min'),
    Output('o3_gauge_pretannage', 'max'),
    Output('o3_gauge_pretannage', 'label')],
    [Input('03_Lot_Ref', 'value'),
    Input('03_Lot_Test', 'value')]
)
def update_o3_gauge_pretannage(lot_ref_name, lot_test_name):
    duree = Foulon1[Foulon1["NomLot"] == lot_test_name].Duree_Pretannage_Tannage.iloc[0] /3600
    duree_ref = (Foulon1[Foulon1["NomLot"] == lot_ref_name].Duree_Pretannage_Tannage.iloc[0]/3600).round(1)
    Low = IC_durrees["Pretannage_Tannage"][2]/3600
    Up = IC_durrees["Pretannage_Tannage"][3]/3600
    Lower = IC_durrees["Pretannage_Tannage"][0]/3600
    Upper = IC_durrees["Pretannage_Tannage"][1]/3600
    min = 0
    maxi = round(max(1.2*Upper, duree), 1)
    default = "#84e61c"
    if((duree < Lower) | (duree > Upper)):
        default = "rgba(242, 144, 24,1)"
        if((duree < Low) | (duree > Up)):
            default = "rgba(228,56,35,1)"    
    color={"gradient":False,"ranges":{"rgba(228,56,35,1)":[min,Low],"rgba(242, 144, 24,1)": [Low,Lower], "#84e61c":[Lower,Upper],"#f29018": [Upper,Up],"red":[Up,maxi]},
           "default":default}
    label = "Prétannage Tannage (Ref = {}h)".format(duree_ref)
    return [duree, color, min, maxi, label]

@app.callback(
    [Output("o3_table_CC", "columns"),
    Output("o3_table_CC", "data"),
    Output("o3_table_CC", "style_data_conditional")],
    [Input("03_Lot_Test", "value"),
    Input("03_Lot_Ref", "value")]
)
def update_o3_table_CC(lot_test_name,lot_ref_name):
    Tab_ref = Alertes[Alertes["NomLot"] == lot_ref_name]
    Tab_test = Alertes[Alertes["NomLot"] == lot_test_name]
    donnees = np.array([
        ["Déchaulage",Tab_ref.Dechaulage_Ph_out_pc.iloc[0],Tab_test.Dechaulage_Ph_out_pc.iloc[0],Tab_ref.Dechaulage_Temp_out_pc.iloc[0], Tab_test.Dechaulage_Temp_out_pc.iloc[0], Tab_ref.Dechaulage_Ph_out.iloc[0],Tab_test.Dechaulage_Ph_out.iloc[0], Tab_ref.Dechaulage_Temp_out.iloc[0], Tab_test.Dechaulage_Temp_out.iloc[0]], 
        ["Pickle",Tab_ref.Pickle_Ph_out_pc.iloc[0],Tab_test.Pickle_Ph_out_pc.iloc[0],Tab_ref.Pickle_Temp_out_pc.iloc[0], Tab_test.Pickle_Temp_out_pc.iloc[0], Tab_ref.Pickle_Ph_out.iloc[0],Tab_test.Pickle_Ph_out.iloc[0],Tab_ref.Pickle_Temp_out.iloc[0], Tab_test.Pickle_Temp_out.iloc[0]], 
        ["Fin Pickle", Tab_ref.Fin_Pickle_Ph_out_pc.iloc[0], Tab_test.Fin_Pickle_Ph_out_pc.iloc[0], Tab_ref.Fin_Pickle_Temp_out_pc.iloc[0], Tab_test.Fin_Pickle_Temp_out_pc.iloc[0], Tab_ref.Fin_Pickle_Ph_out.iloc[0], Tab_test.Fin_Pickle_Ph_out.iloc[0], Tab_ref.Fin_Pickle_Temp_out.iloc[0], Tab_test.Fin_Pickle_Temp_out.iloc[0]], 
        ["Prétannage - Tannage", Tab_ref.Pretannage_Tannage_Ph_out_pc.iloc[0],Tab_test.Pretannage_Tannage_Ph_out_pc.iloc[0],Tab_ref.Pretannage_Tannage_Temp_out_pc.iloc[0],Tab_test.Pretannage_Tannage_Temp_out_pc.iloc[0],Tab_ref.Pretannage_Tannage_Ph_out.iloc[0],Tab_test.Pretannage_Tannage_Ph_out.iloc[0],Tab_ref.Pretannage_Tannage_Temp_out.iloc[0], Tab_test.Pretannage_Tannage_Temp_out.iloc[0]]])
    index = ["Déchaulage", "Pickle", "Fin de Pickle", "Prétannage / Tannage"]
    df = pd.DataFrame(donnees,index=index, columns=["","pH_ref_pc","pH_test_pc", "Température_ref_pc","Température_test_pc", "pH_ref", "pH_test", "Température_ref", "Température_test"])

    columns=[
        {"name": ["",""], "id": ""},
        {"name": ["pH", "Ref"], "id": "pH_ref_pc"},
        {"name": ["pH", "Test"], "id": "pH_test_pc"},
        {"name": ["Température", "Ref"], "id": "Température_ref_pc"},
        {"name": ["Température", "Test"], "id": "Température_test_pc"}
    ]
    data=df.to_dict('records')
    
    style_data_conditional=(
        [
                    {
                    'if': {
                    'filter_query': '{{{col}}} > 50'.format(col=col),
                    'column_id': col + "_pc"
                    },
                    'color': 'rgba(228,56,35,1)'
                    } for col in df.columns[5:9]
                ] +
                [
                    {
                    'if': {
                    'filter_query': '{{{col}}} = 0'.format(col=col),
                    'column_id': col + "_pc"
                    },
                    'color': 'rgba(132, 230, 28,1)'
                    } for col in df.columns[5:9]

                ]+
                [
                    {
                    'if': {
                    'filter_query': '{{{col}}} > 0 && {{{col}}} <= 50'.format(col=col),
                    'column_id': col + "_pc"
                    },
                    'color': 'rgba(242, 144, 24,1)'
                    } for col in df.columns[5:9]
                ])
    #columns[1]["name"] = "pH",
    #columns[2]["name"] = "Température",
    return [columns, data, style_data_conditional]

@app.callback(
    [Output('o3_Dechaulage_Detect', 'color'),
    Output('o3_Pickle_Detect', 'color'),
    Output('o3_FinPickle_Detect', 'color'),
    Output('o3_Pretannage_Detect', 'color')],
    [Input('03_Lot_Test', 'value')]
)

def update_o3_Period_Detect_indicators(lot_test_name):
    Col_Dech = "#84e61c"
    Col_Pickle= "#84e61c"
    Col_FinPickle= "#84e61c"
    Col_Pretannage= "#84e61c"
    if Alertes[Alertes["NomLot"] == lot_test_name].Dechaulage_detect.bool() == False:
        Col_Dech = "rgba(228,56,35,1)"
    if Alertes[Alertes["NomLot"] == lot_test_name].Pickle_detect.bool()== False:
        Col_Pickle = "rgba(228,56,35,1)"
    if Alertes[Alertes["NomLot"] == lot_test_name].Fin_Pickle_detect.bool()== False:
        Col_FinPickle = "rgba(228,56,35,1)"
    if Alertes[Alertes["NomLot"] == lot_test_name].Pretannage_Tannage_detect.bool()== False:
        Col_Pretannage = "rgba(228,56,35,1)"
    return [Col_Dech, Col_Pickle, Col_FinPickle, Col_Pretannage]

if __name__ == '__main__':
    #app.run_server(host="0.0.0.0", debug=True)
    app.run_server(debug=True)